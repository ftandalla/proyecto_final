<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fun extends Model
{
    //use HasFactory;
    protected $table="funcionalidad";
    protected $primaryKey="COD_FUNCIONALIDAD";
    protected $fillable = ['cod_rol', 'nom_funcionalidad', 'det_funcionalidad'];

    public $timestamps = false;
}
