<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
  //use HasFactory;
protected $table="users";
protected $primaryKey="CI_USUARIO";
protected $fillable = ['CI_USUARIO', 'cod_rol', 'nom_usuario', 'email', 'fon_usuario', 'dir_usuario', 'password', 'tipo_usuario', 'factor_usuario'];

public $timestamps = false;
}
