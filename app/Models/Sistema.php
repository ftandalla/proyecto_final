<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
  protected $table="sistema";
  protected $primaryKey="COD_SISTEMA";
  protected $fillable = ['cod_sistema','nom_sistema', 'desc_sistema', 'est_sistema'];

  public $timestamps = false;
}
