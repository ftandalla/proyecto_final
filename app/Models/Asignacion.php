<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    protected $table="asignacion";
    protected $primaryKey="COD_ASIGNACION";
    protected $fillable = ['ci_usuario', 'cod_rol'];
  
    public $timestamps = false;
}


