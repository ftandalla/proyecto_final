<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //use HasFactory;
    protected $table="rol";
    protected $primaryKey="cod_rol";
    protected $fillable = ['cod_sistema', 'nom_rol', 'area_rol', 'det_rol'];

    public $timestamps = false;
}
