<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
  protected $table="rol";
  protected $primaryKey="COD_ROL";
  protected $fillable = ['cod_sistema', 'nom_rol',  'det_rol'];

  public $timestamps = false;
}
