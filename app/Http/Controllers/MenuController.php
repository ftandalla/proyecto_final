<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Rol;
use App\Models\Sistema;
use App\Models\Asignacion;
use App\Models\Fun;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
      
      $sistemas=Sistema::all();
      $sistema = session('sistema');
      $roles=Rol::where('cod_sistema', $sistema)->get();
      $sistem=Sistema::findOrFail($sistema);
      return view('rol', compact('roles','sistemas','sistem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getData(Request $req)
    {
      $sistemas=Sistema::all();
      $roles=Rol::all();
      return redirect()->action([MenuController::class, 'index']);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'nombre' => 'required',
        'descripcion' => 'required'
    ]);

        $roles= new Rol;
        $sistema = session('sistema');
        
        $roles->cod_sistema=$sistema;
        $roles->nom_rol=$request->input('nombre');
        $roles->det_rol=$request->input('descripcion');
        if($roles->save()){
            Session::flash('rol_creado', 'El Rol ha sido creado exitosamente');
          }

        return redirect()->route('rol.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sistema = session('sistema');
        $sistem=Sistema::findOrFail($sistema);
        $roles= Rol::findOrFail($id);
        return view('editRol', compact('roles','sistem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $request->validate([
        'codigo2' => 'required',
        'nombre2' => 'required',
        'descripcion2' => 'required'
    ]);

        $roles=Rol::findOrFail($id);
        $sistema = session('sistema');
        $roles->cod_rol=$request->input('codigo2');
        $roles->cod_sistema=$sistema;
        $roles->nom_rol=$request->input('nombre2');
        $roles->det_rol=$request->input('descripcion2');
        
        if($roles->save()){
            Session::flash('rol_actualizado', 'El Rol ha sido editado exitosamente');
          }
        return redirect()->route('rol.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $asignacion=Asignacion::where('cod_rol', $id)->delete();
        $funciones=Fun::where('cod_rol', $id)->delete();
          
          if($roles = Rol::where('cod_rol', $id)->delete()){
            Session::flash('rol_eliminado', 'El Rol ha sido eliminado exitosamente');
          }

        return redirect()->route('rol.index');
    }
}
