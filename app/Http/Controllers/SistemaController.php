<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Sistema;
use App\Models\Rol;

class SistemaController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $sistemas=Sistema::all();
      return view('layouts.principal', compact('sistemas'));
    }

    public function getData(Request $req)
    {
      $sistemas=Sistema::all();
      $roles=Rol::all();
      return redirect('rol');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      return view('crearSistema');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $sis_id =$request->input('sistema');
      
      if(Sistema::where('nom_sistema', $sis_id )->exists()){
        Session::flash('sistema_duplicado', 'El Sistema  ya existe');
        return redirect()->route('sistema.index');
      }

      $sistemas= new Sistema;
      $sistemas->nom_sistema=$request->input('sistema');
      $sistemas->desc_sistema=$request->input('descripcion');
      $sistemas->est_sistema=$request->input('estado');
      if($sistemas->save()){
        Session::flash('sistema_creado', 'El sistema a sido creado exitosamente');
      }
      return redirect()->route('sistema.index');
    }


    public function irasistema(Request $request)
    {
      
      $sistema = $request->input('sis');
      session()->put('sistema', $sistema);

      return redirect()->route('accesos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
