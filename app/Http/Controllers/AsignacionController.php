<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Asignacion;
use App\Models\Rol;
use App\Models\Usuario;
use App\Models\Sistema;

class AsignacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'cedula' => 'required',
            'rol1' => 'required'
        ]);

        $asign= new Asignacion;
        $asign->CI_USUARIO=$request->input('cedula');
        $asign->COD_ROL=$request->input('rol1');
        
        if($asign->save()){
            Session::flash('asignacion_creada', 'La Asignación a sido creada exitosamente');
            return redirect()->route('usuario.index');
          }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sistema = session('sistema');
        $sistem=Sistema::findOrFail($sistema);
        
        $usuario=Usuario::where('ci_usuario', $id)->get();
        $asignacion=Asignacion::where('ci_usuario', $id)->get();
        $roles=Rol::where('cod_sistema', $sistema)->get();
        return view('asignacion', compact('asignacion', 'roles', 'usuario', 'sistem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asignacion=Asignacion::findOrFail($id);
        if(empty($asignacion)){
            return redirect()->route('usuario.index');
          }

          if($asignacion->delete()){
            Session::flash('asignacion_eliminada', 'La Asignación a sido eliminada exitosamente');
            return redirect()->route('usuario.index');
          }
        
        
    }
}
