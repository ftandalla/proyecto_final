<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Usuario;
use App\Models\Rol;
use App\Models\Asignacion;
use App\Models\Sistema;

use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$roles=Rol::all();
        $sistema = session('sistema');
        $sistem=Sistema::findOrFail($sistema);
        $usuarios=usuario::all();
        return view('usuario', compact( 'usuarios', 'sistem'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
        'cedula' => 'required',
        'nombre' => 'required',
        'correo' => 'required',
        'telefono' => 'required',
        'direccion' => 'required',
        'pass' => 'required',
        'tipo' => 'required'
    ]);

    $user_id =$request->input('cedula');

    if (Usuario::where('ci_usuario', $user_id )->exists()) {
      Session::flash('usuario_duplicado', 'El Usuario con dicha cedula ya existe');
      return redirect()->route('usuario.index');
   }
    

      $correo=$request->input('correo');

      if (Usuario::where('email', $correo )->exists()) {
        Session::flash('correo_duplicado', 'El Correo ya se encuentra registrado');
        return redirect()->route('usuario.index');
     }

      //$usuarios=usuario::findOrFail($usuario);
      $usuarios= new Usuario;
      $usuarios->ci_usuario=$request->input('cedula');
      //$usuarios->cod_rol=$request->input('rol');
      $usuarios->NOM_USUARIO=$request->input('nombre');
      $usuarios->email=$request->input('correo');
      $usuarios->FON_USUARIO=$request->input('telefono');
      $usuarios->DIR_USUARIO=$request->input('direccion');

      //Encrypt
      $usuarios->password=Hash::make($request->input('pass'));

      $usuarios->tipo_usuario=$request->input('tipo');
      
      if($usuarios->save()){
        Session::flash('usuario_creado', 'El Usuario a sido creado exitosamente');
        return redirect()->route('usuario.index');
      }
      
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //$roles=Rol::all();
      $usuarios= Usuario::findOrFail($id);
      return view('editUsuario', compact('usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'cedula' => 'required',
        'nombre' => 'required',
        'correo' => 'required',
        'telefono' => 'required',
        'direccion' => 'required',
        'contraseña' => 'required',
        'tipo' => 'required'
    ]);

      $usuarios=Usuario::findOrFail($id);

      $usuarios->NOM_USUARIO=$request->input('nombre');
      $usuarios->email=$request->input('correo');
      $usuarios->FON_USUARIO=$request->input('telefono');
      $usuarios->DIR_USUARIO=$request->input('direccion');
      $usuarios->password=Hash::make($request->input('contraseña'));
      $usuarios->TIPO_USUARIO=$request->input('tipo');

      if($usuarios->save()){
        Session::flash('usuario_actualizado', 'El Usuario a sido editado exitosamente');
        return redirect()->route('usuario.index');
      }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      //$usuarios = Usuario::where('ci_usuario', $id)->delete();
      $asignacion=Asignacion::where('ci_usuario', $id)->delete();
      $usuarios=Usuario::findOrFail($id);

      if(empty($usuarios)){
        return redirect()->route('usuario.index');
      }

      ;
      if(Usuario::where('ci_usuario', $id)->delete()){
        Session::flash('usuario_eliminado', 'El usuario a sido eliminado exitosamente');
      }

      //$roles->delete(Rol->$id);
      //$roles->delete();
      return redirect(route('usuario.index'));

    }
}
