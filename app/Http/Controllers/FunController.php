<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Fun;
use App\Models\Rol;
use App\Models\Sistema;

class FunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sistema = session('sistema');
      $sistem=Sistema::findOrFail($sistema);
      $roles=Rol::where('COD_SISTEMA', $sistema)->get();
      $funciones=Fun::all();
      return view('permiso', compact('roles', 'funciones', 'sistem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
        'funcion' => 'required',
        'descripcion' => 'required',
        'rol' => 'required'
    ]);

    
      $funciones= new Fun;
      $funciones->NOM_FUNCIONALIDAD=$request->input('funcion');
      $funciones->cod_rol=$request->input('rol');
      $funciones->DET_FUNCIONALIDAD=$request->input('descripcion');
      if($funciones->save()){
        Session::flash('funcionalidad_creada', 'La funcionalidad ha sido creado exitosamente');
      }
      return redirect()->route('permiso.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sistema = session('sistema');
      $funciones= Fun::findOrFail($id);
      $roles=Rol::where('COD_SISTEMA', $sistema)->get();
      return view('editPermiso', compact('roles', 'funciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $request->validate([
        'funcion' => 'required',
        'descripcion' => 'required',
        'rol' => 'required'
    ]);

      $funciones=Fun::findOrFail($id);
      $funciones->NOM_FUNCIONALIDAD=$request->input('funcion');
      $funciones->DET_FUNCIONALIDAD=$request->input('descripcion');
      
      if($funciones->save()){
        Session::flash('funcionalidad_actualizado', 'La Funcionalidad ha sido editada exitosamente');
      }
      return redirect()->route('permiso.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $funciones=Fun::findOrFail($id);
      
      if($funciones->delete()){
        Session::flash('funcionalidad_eliminada', 'La Funcionalidad ha sido eliminada exitosamente');
      }
        return redirect()->route('permiso.index');
    }
}
