<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\SistemaController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\FunController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\AsignacionController;
use App\Http\Controllers\AccesosController;
use App\Http\Controllers\Auth\TwoFactorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('welcome');


Route::get('/', function () {
    return view('auth.login');
    
});

Route::get('verify/resend', [TwoFactorController::class, 'resend'])->name('verify.resend');
Route::resource('verify', TwoFactorController::class)->only(['index', 'store']);

Auth::routes();

//Route::resource('/usuario',UsuarioController::class);

Route::group(['middleware' => ['auth', 'twofactor']], function () {
   
    Route::resource('/sistema',SistemaController::class);

    Route::post('irasistema',[SistemaController::class, 'irasistema']);

    Route::resource('/rol',MenuController::class);

    Route::resource('/crearSistema',SistemaController::class);

    Route::resource('/editRol',MenuController::class);

    Route::resource('/permiso',FunController::class);

    Route::resource('/usuario',UsuarioController::class);

    Route::resource('/editPermiso',FunController::class);

    Route::resource('/cliente',ClienteController::class);

    Route::resource('/inicio',InicioController::class);

    Route::resource('/asignacion',AsignacionController::class);

    Route::resource('/accesos',AccesosController::class);

    

});


