@extends('layouts.app')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ACCESOS</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
      

    </head>
    <body class="antialiased">



<div class="jumbotron ">
          <h1>SISTEMA DE GESTIÓN DE CONTROL DE ACCESOS</h1>
          <h2>Bienvenido</h2>
        </div>

        @if(Session::has('sistema_creado'))
        <div class="alert alert-success">
        <strong>Éxito!</strong> 
        
        {{session('sistema_creado')}}
        
        </div>
        @endif

        @if(Session::has('sistema_duplicado'))
        <div class="alert alert-danger">
        <strong>Error!</strong> 
        
        {{session('sistema_duplicado')}}
        
        </div>
      @endif

        <div class="container">
            <div class="row justify-content-center">

            
              <form class="col-md-8" action="/irasistema" method="POST">
              @csrf                 
                      <div class="form-group ">
                        <label for="sel1">Seleccione un sistema a gestionar:</label>

                        <select class="form-control" name="sis">
                          @foreach($sistemas as $sistema)
                            <option value = '{{ $sistema->COD_SISTEMA }}' >{{$sistema->NOM_SISTEMA}}</option>
                          @endforeach

                        </select>
                      </div>
                      <button type="submit" class="btn btn-success">
                      Ingresar
                      </button>
                      
                      <a class="btn btn-info" href="{{route('sistema.create')}}">Nuevo</a>

       </form>
       

     </div>
      </div>


    </body>
</html>
