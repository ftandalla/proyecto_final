@extends('layouts.app')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Accesos</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
    </style>

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>

  </head>
  <body class="antialiased">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="/accesos">Accesos</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse"
  data-target="#navbarSupportedContent" aria-controls="navbarNavDropdown"
  aria-expanded="false" aria-label="Toggle navigation" name="button">
  <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"><a class="nav-link" href="/rol">Rol</a></li>
      <li class="nav-item active"><a class="nav-link" >Funcionalidad</a></li>
      <li class="nav-item"><a class="nav-link" href="/usuario">Usuarios</a></li>
    </ul>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" >
      @csrf
      <button type="submit" class="btn btn-link">
        
      Cerrar Sesion
      </button>
       
      </form>

  </div>
</nav>

<div class="jumbotron">
  <br>
<h1>SISTEMA DE GESTIÓN DE CONTROL DE ACCESOS</h1>
</div>

<div class="container">
<h2>{{$sistem->NOM_SISTEMA}}</h2>
</div>

@if(Session::has('funcionalidad_creada'))
        <div class="alert alert-success">
        <strong>Éxito!</strong> 
        
        {{session('funcionalidad_creada')}}
        
        </div>
@endif

@if(Session::has('funcionalidad_eliminada'))
        <div class="alert alert-warning">
        <strong>Éxito!</strong> 
        
        {{session('funcionalidad_eliminada')}}
        
        </div>
@endif

@if(Session::has('funcionalidad_actualizado'))
        <div class="alert alert-success">
        <strong>Éxito!</strong> 
        
        {{session('funcionalidad_actualizado')}}
        
        </div>
@endif

<div class="container-fluid">

<ul class="nav nav-tabs" id="myTab" role="tablist">

  <li class="nav-item">
    <a class="nav-link active" id="Listado-tab"
    data-toggle="tab" href="#Listado" role="tab"
    aria-controls="Listado" aria-selected="true">Listado</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " id="Crear-tab"
    data-toggle="tab" href="#Crear" role="tab"
    aria-controls="Crear" aria-selected="true">Crear</a>
  </li>

</ul>

<div class="tab-content"  id="myTabContent">

  <div class="tab-pane fade show active" id="Listado" role="tabpanel"
    aria-labelledby="Listado-tab">
    <br>
  <h5>Funcionalidades</h5>
    <div class="row">
      <div class="col-xl-12">
        <div class="table-responsive">
            <table class="table table-stripped">
              <thead>
                <tr>
                  <th>Rol Asociado</th>
                  <th>Funcionalidad</th>
                  <th>Detalle</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($funciones as $fun)
                @isset($roles->find($fun->COD_ROL)->NOM_ROL)
                  <tr>
                    <td>{{$roles->find($fun->COD_ROL)->NOM_ROL}}</td>
                    <td>{{$fun->NOM_FUNCIONALIDAD}}</td>
                    <td>{{$fun->DET_FUNCIONALIDAD}}</td>
                    <td>
                      <a href="{{route('permiso.edit', $fun->COD_FUNCIONALIDAD)}}" class="btn btn-primary  btn-sm form-group col-sm-6">Editar</a>
                      <form action="{{route('permiso.destroy', $fun->COD_FUNCIONALIDAD)}}"  method="post" >
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-sm form-group col-sm-6" value="Eliminar">
                      </form>
                      </td>
                  </tr>
                  @endisset
                @endforeach
              </tbody>
            </table>
        </div>
        </table>
      </div>
  </div>
    </div>

    <div class="tab-pane fade show" id="Crear" role="tabpanel"
    aria-labelledby="Crear-tab">
    <br>
    <form action="{{route('permiso.store')}}" method="post">
      @csrf
<div class="form-row col-sm-12">
 <div class="form-group col-sm-6">
   <label for="funcion">Funcioanalidad</label>
   <input type="text" class="form-control" id="funcion"
    name="funcion" size="20" placeholder="función" required>
    @error('funcion')
        <br>
        <small>*{{$message}}</small>
        <br>
        @enderror
 </div>
 <div class="form-group col-sm-6">
   <label for="rol">Rol</label>
   <select name="rol" id="rol" class="custom-select mb-3">
     @foreach ($roles as $rol)
     <option value="{{$rol->COD_ROL}}">{{$rol->NOM_ROL}}</option>
     @endforeach
      </select>

 </div>
</div>

<div class="form-row col-sm-12">
<div class="form-group col-sm-6">
 <label for="descripcion">Descripción</label>
 <input type="text" class="form-control" id="descripcion"
  name="descripcion" size="50" 
  placeholder="Ingrese la descripción" required>
  @error('descripcion')
        <br>
        <small>*{{$message}}</small>
        <br>
        @enderror
</div>
</div>

<div class="form-row col-sm-12">
 <button type="submit" class="btn btn-primary">Crear</button>
 </div>
</div>
</form>
    </div> </div>
</div>
  </body>
</html>
